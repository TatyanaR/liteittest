
//подключаем внешние модули
requirejs.config({
    // baseUrl: 'js/lib',
    paths: {

        jquery: 'lib/jquery.min'

    }
});


// инициализируем экземпляр главного приложения Angular
var dktApp = angular.module('dktApp', ['ngRoute']);
dktApp.model = dktApp.model || {};
dktApp.model.user = {
    login: 'Гость',
    token: '',
    success: false
};


// =============================================================================



// filter to show html as is
dktApp.filter("asHtml", ['$sce', function ($sce) {
        return function (value, type) {
            return $sce.trustAs(type || 'html', value);
        };
    }]);


dktApp.config(['$routeProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$locationProvider', '$provide', '$httpProvider',
    function ($routeProvider, $controllerProvider, $compileProvider, $filterProvider, $locationProvider, $provide, $httpProvider) {

// настраиваем роутинг к расположения страниц Angular
        var routeFactory = function (path) {
            return {
                templateUrl: 'view/' + path + '.html',
                resolve: {load: ['$q', '$rootScope', function ($q, $rootScope) {
                            var deferred = $q.defer();
                            require(['view/' + path], function () {
                                $rootScope.$apply(function () {
                                    deferred.resolve();
                                });
                            });
                            return deferred.promise;
                        }]}
            };
        };
// перенаправление
        $routeProvider
                .when('/', routeFactory('list/list'))
                .when('/product/:Id', routeFactory('product/product'))

                //               .when('/promotion/view/:Id', routeFactory('promotion/view'))

                .otherwise(routeFactory('404'));

        $locationProvider.html5Mode({enable: true});

        dktApp.register = {
            controller: $controllerProvider.register,
            directive: $compileProvider.directive,
            filter: $filterProvider.register,
            factory: $provide.factory,
            service: $provide.service
        };

    }]);




dktApp.controller('MainCtrl', [
    '$scope', '$http', '$timeout',
    function ($scope, $http, $timeout) {
        $scope.Loggeduser = dktApp.model.user;
        $scope.message = '';
        $scope.user = {
            login: '',
            password: ''
        };
        //***************  Общие функции ******************************      
        dktApp.model.httpget = function (route, data, onsuccess, onerror) {

            $http.get(window.config.backendHost(route, []),
                    data)
                    .success(function (result) {
                        if (typeof (onsuccess) === 'function')
                            onsuccess(result);
                        return;
                    })

                    .error(function (result) {
                        if (typeof (onerror) === 'function') {
                            onerror(result);
                        }
                    });
        };


        dktApp.model.httppost = function (route, data, onsuccess, onerror) {

            $http.post(window.config.backendHost(route, {}), data)
                    .success(function (result) {
                        if (typeof (onsuccess) === 'function')
                            onsuccess(result);
                        return;
                    })

                    .error(function (result) {
                        if (typeof (onerror) === 'function') {
                            onerror(result);
                        }
                    });
        };

        dktApp.model.httppostToken = function (route, data, onsuccess, onerror) {

            $http.post(window.config.backendHost(route, {}), data,
                    {headers: {'Authorization': 'Token ' + dktApp.model.user.token}})
                    .success(function (result) {
                        if (typeof (onsuccess) === 'function')
                            onsuccess(result);
                        return;
                    })

                    .error(function (result) {
                        if (typeof (onerror) === 'function') {
                            onerror(result);
                        }
                    });
        };

        dktApp.model.ShowMessage = function (message) {
            $scope.message = message;
            $("#messagedialog").dialog({

                title: "Cообщение",
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 300,
                modal: true,
                buttons: {
                    "Ok": function () {

                        $(this).dialog("close");
                    }
                }


            }).dialog('open');
        };
//******************************************************************
        var Login = function (username, passw) { //вход в аккаунт
            dktApp.model.httppost(
                    '/api/login/',
                    {
                        username: username,
                        password: passw
                    },
                    function (result)
                    {
                        console.log(result.success);
                        console.log(result.message);
                        if (!result.success)
                        {
                            dktApp.model.user.succes = false;
                            dktApp.model.user.login = '';
                            dktApp.model.user.token = '';
                            dktApp.model.ShowMessage('Неправильные учетные данные или Вы незарегистрированы! Зарегистрируйтесь!');
                        } else
                        {
                            //регистрация прошла
                            dktApp.model.user.login = username;
                            dktApp.model.user.token = result.token;
                            dktApp.model.user.success = true;
                            $scope.Loggeduser = dktApp.model.user;

                        }
                    },
                    function (result)
                    {
                        console.log('login failed');
                    }
            );
        };

        $scope.LogOut = function (username, passw) { //выход из аккаунта
            dktApp.model.httppost(
                    '/api/logout/',
                    {
                        username: username
                    },
                    function (result)
                    {
                        console.log(result.success);
                        console.log(result.message);
                        if (!result.success)
                        {

                            console.log('logout failed');
                        } else
                        {
                            //регистрация прошла
                            dktApp.model.user = {
                                login: 'Гость',
                                token: '',
                                success: false
                            };

                            $scope.Loggeduser = dktApp.model.user;
                            console.log('logout ');
                        }
                    },
                    function (result)
                    {
                        console.log('logout failed');
                    }
            );
        }
        ;


        $scope.ShowLoginDlg = function () //показываем диалог для логина с паролем
        {
            $scope.user.login = '';
            $scope.user.password = '';
            $("#logindialog").dialog({

                title: "Вход",
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 300,
                modal: true,
                buttons: {
                    "Ok": function () {
                        Login($scope.user.login, $scope.user.password);
                        $(this).dialog("close");
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            }).dialog('open');
        };

        var RegisterNew = function (username, passw) { //регистрация нового пользователя
            dktApp.model.httppost(
                    '/api/register/',
                    {
                        username: username,
                        password: passw
                    },
                    function (result)
                    {
                        console.log(result.success);
                        console.log(result.message);
                        if (!result.success)
                        {
                            dktApp.model.user.succes = false;
                            dktApp.model.user.login = '';
                            dktApp.model.user.token = '';
                            $scope.Loggeduser = dktApp.model.user;

                            dktApp.model.ShowMessage('Неудачная регистрация. Проверьте учетные данные!');
                        } else
                        {
                            //регистрация прошла
                            dktApp.model.user.login = result.username;
                            dktApp.model.user.token = result.token;
                            dktApp.model.user.success = true;
                            $scope.Loggeduser = dktApp.model.user;
                        }
                    },
                    function (result)
                    {
                        console.log('login failed');
                    }
            );
        };


        $scope.ShowRegisterDlg = function () //показываем диалог для регистрации
        {
            $scope.user.login = '';
            $scope.user.password = '';
            $("#logindialog").dialog({

                title: "Регистрация",
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 300,
                modal: true,
                buttons: {
                    "Ok": function () {
                        if(($scope.user.login.trim().length>0)&&($scope.user.password.trim().length>0))
                        {  RegisterNew($scope.user.login.trim(), $scope.user.password.trim());
                        $(this).dialog("close");
                        }
                        else alert('Заполните все поля!');
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            }).dialog('open');
        };





        $timeout(function () {
            //DOM has finished rendering

            $(document).click(function (event) {
                // console.log(event.currentTarget);


            });
        });
    }]);
// глобальные полезняшки

function FindIndexOf(massiv, id, idvalue)
{
    for (var i = 0; i < massiv.length; i++)
    {

        if (massiv[i][id] == idvalue) //может быть сравнение строки с числом!
            return i;
    }
    console.log('Error: not find index');
    return -1; //
}