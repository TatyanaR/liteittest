dktApp.register.controller('ProductCtrl', ['$scope', '$http', '$timeout', '$routeParams', '$location',
    function ($scope, $http, $timeout, $routeParams, $location) {
        /*
         * [
         {
         "id": 10,
         "product": 1,
         "created_by": {
         "id": 1,
         "username": "user"
         },
         "rate": 0,
         "text": "ipsum"
         },
         ...
         ]
         */
        /*  $scope.product={
         "id": 1,
         "img": "img1.png",
         "text": "Lorem ipsum dolor sit amet.",
         "title": "product 1"
         };*/
        $scope.reviews = [];
        $scope.vote = { //данные для отправки на сервер при голосовании
            rate: 0,
            text: ""};
        
        $scope.Loggeduser = dktApp.model.user;
        $scope.config = window.config;

        //массив из ссылок на рисунки звезд для голосовалки
        $scope.Stars = [];

        function ResetStars()
        {
            // заполним массив из ссылок на рисунки звезд для голосовалки "пустыми"
            //звездами
            for (var i = 0; i < 5; i++)
                $scope.Stars[i] = 'img/star-dark.png';
            $scope.vote.rate = 0;
        }
        ;

        ResetStars();

        $scope.OnRateStar = function (rate) // щелкаем по звезде
        {
            // заполним массив из ссылок на рисунки звезд до щелкнутой  "яркими"
            for (var i = 0; i <= rate; i++)
                $scope.Stars[i] = 'img/star-light.png';
            // заполним массив из ссылок на рисунки звезд до конца  "пустыми"
            for (var i = rate + 1; i < 5; i++)
                $scope.Stars[i] = 'img/star-dark.png';
            $scope.vote.rate = rate + 1;
        };


        $scope.back = function () { //переход назад по клику
            $location.url('/');
        };

        $scope.getNumber = function (N)
        {
            return new Array(N);
        };

        function GetReviews(productid)//получить отзывы на данный продукт
        {
            dktApp.model.httpget("/api/reviews/" + productid, [], 
                    function (reviews) {
                        $scope.reviews = reviews;
                        console.log('review succes');
                        $scope.reviews.sort( function(b,a){ //упорядочим по дате
                                                 var a1 =new Date( a.created_at);
                                                 var b1 = new Date(b.created_at);
                                                 if (a1==b1) return 0;
                                                 if (a1<b1) return -1;
                                                 if (a1>b1) return 1;
                                                   });
                    },
                    function (data) {
                        console.log('review error');
                    }
            );
        }
        //получаем список продуктов
        dktApp.model.httpget("/api/products/?format=json", [],
                function (products) {
                    //берем выбранный    
                    $scope.product = products[  FindIndexOf(products, 'id', $routeParams.Id)];
                    GetReviews($routeParams.Id);//получить отзывы на данный продукт
                    console.log('list succes');

                },
                function (data) {
                    console.log('list error')
                }
        );


        $scope.onSend = function () //отсылаем отзыв с оценкой
        {
            dktApp.model.httppostToken("/api/reviews/" + $routeParams.Id, $scope.vote, //послать отзыв на данный продукт
                    function (result) {
                        ResetStars();
                        $scope.vote = {
                            rate: 0,
                            text: ""};
                         GetReviews($routeParams.Id); //перезагрузим отзывы
                        console.log('post review succes' + result.review_id);
                    },
                    function (data) {
                        console.log('post review error');
                    }
            );

        };

        $timeout(function () {




        });
    }]);